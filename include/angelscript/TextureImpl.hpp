/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ANGELSCRIPT_TEXTUREIMPL_HPP
#define ANGELSCRIPT_TEXTUREIMPL_HPP

#include <SFML/System/Vector2.hpp>

#include <string>
#include <map>

namespace angelscript
{
    
class TextureImpl
{
public:
    using Vec2 = std::pair<unsigned int, unsigned int>;
public:
    explicit TextureImpl() noexcept;
    TextureImpl(const TextureImpl& other) noexcept;
    TextureImpl& operator=(const TextureImpl& other) noexcept;

    bool has(const std::string& side) noexcept;
    void set(const std::string& side,
             unsigned int offX, unsigned int offY) noexcept;
    void setAll(unsigned int offX, unsigned int offY) noexcept;
    void push(const std::string& side) noexcept;
    void clear() noexcept;

    std::map<std::string, sf::Vector2f> data() const noexcept;
public:
    friend bool textureimpl_has(const std::string& side,
                                TextureImpl* impl) noexcept;
    friend void textureimpl_set(const std::string& side,
                                unsigned int offX, unsigned int offY,
                                TextureImpl* impl) noexcept;
    friend void textureimpl_setAll(unsigned int offX, unsigned int offY,
                                   TextureImpl* impl) noexcept;
    friend void textureimpl_push(const std::string& side,
                                 TextureImpl* impl) noexcept;
    friend void textureimpl_clear(TextureImpl* impl) noexcept;
public:
    mutable std::map<std::string, Vec2> map;
};

void textureimpl_construct(void* mem);
void textureimpl_destruct(void* mem);
bool textureimpl_has(const std::string& side,
                     TextureImpl* impl) noexcept;
void textureimpl_set(const std::string& side,
                     unsigned int offX, unsigned int offY,
                     TextureImpl* impl) noexcept;
void textureimpl_setAll(unsigned int offX, unsigned int offY,
                        TextureImpl* impl) noexcept;
void textureimpl_push(const std::string& side,
                      TextureImpl* impl) noexcept;
void textureimpl_clear(TextureImpl* impl) noexcept;

}

#endif /* ANGELSCRIPT_TEXTUREIMPL_HPP */
