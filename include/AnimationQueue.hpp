/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ANIMATIONQUEUE_HPP
#define ANIMATIONQUEUE_HPP

#include "Animator.hpp"

#include <queue>

template<class T, class KeyT = std::string>
class AnimationQueue
{
public:
    static_assert(!(std::is_floating_point<KeyT>::value),
                  "KeyT must not be a floating point as != is used.");
public:
    explicit AnimationQueue(Animator<T, KeyT>& toAnimate) noexcept
        {

        }

    void add(const KeyT& key) noexcept
        {
            animations.push(key);
        }
    
    void update() noexcept
        {
            if(!animator.isPlaying() && !animations.empty())
            {
                animator.play(animations.front(), false);
                animations.pop();
            }
        }
private:
    std::queue<KeyT> animations;
    Animator<T, KeyT>& animator;
};

#endif /* ANIMATIONQUEUE_HPP */
