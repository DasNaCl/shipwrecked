/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SCRIPTENTITY_HPP
#define SCRIPTENTITY_HPP

#include <SFML/System/Vector3.hpp>

class ScriptEntity
{
public:
    void moveX(int x) noexcept;
    void moveY(int y) noexcept;
    void moveZ(int z) noexcept;

    sf::Vector3i moveRequest() const noexcept;
    void set(sf::Vector3i v) noexcept;
public:
    void addRef() noexcept {}
    void rmRef() noexcept {}
private:
    sf::Vector3i toMove;
};

#endif /* SCRIPTENTITY_HPP */
