/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ISOMETRIC_MAP_HPP
#define ISOMETRIC_MAP_HPP

#include "EntityInformation.hpp"
#include "RenderHelper.hpp"
#include "TileResource.hpp"
#include "TilesetData.hpp"
#include "Coordinate.hpp"
#include "Entity.hpp"
#include "Tile.hpp"

#include <SFML/Graphics/Rect.hpp>

#include <SFML/System/Vector3.hpp>
#include <SFML/System/Time.hpp>

#include <memory>
#include <vector>
#include <map>

class IsometricMap : public sf::Drawable, public sf::Transformable
{
public:
    explicit IsometricMap(sf::Vector2u size) noexcept;

    template<typename T>
    Entity& add(T factory)
    {
        entities.emplace_back(factory());
        return *(entities.back());
    }

    sf::Vector3i playerPos() const noexcept;
    std::vector<EntityInformation>& entityTypes() noexcept;

    void update(const sf::Time& dt, const sf::Vector3i& playerpos,
                const sf::Vector2i& mouseWorldPos,
                const sf::Vector2i& mouseViewPos,
                const sf::Vector2i& mousePixelPos) noexcept;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const;
private:
    mutable std::map<Coordinate, Tile> data;
    sf::Vector2u dim;
    sf::Vector3i player;
    TilesetData tilesetData;
    std::vector<Entity::Ptr> entities;

    std::vector<sf::Vector3i> path;

    RenderHelper renderHelper;
    sf::Time entityUpdateRate;
    sf::Time total;

    TileResource tileResource;

    std::vector<EntityInformation> entityDummies;
};

#endif /* ISOMETRIC_MAP_HPP */
