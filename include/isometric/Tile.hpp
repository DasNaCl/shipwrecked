/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ISOMETRIC_TILE_HPP
#define ISOMETRIC_TILE_HPP

#include "TextureImpl.hpp"

#include "isometric/TileRenderer.hpp"

#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Color.hpp>

#include <SFML/System/Vector3.hpp>

#include <string>

class Tile : public sf::Drawable, public sf::Transformable
{
public:
    static int width() noexcept;
    static int height() noexcept;
public:
    explicit Tile(const std::string& type,
                  TileRenderer tileRenderer,
                  const TextureImpl& defaultTextures) noexcept;

    void setMapPosition(const sf::Vector3i& pos) noexcept;
    const sf::Vector3i& getMapPosition() const noexcept;

    void setOffset(const sf::Vector2f& off) noexcept;

    std::string getKey() const noexcept;

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    void updatePos() noexcept;
private:
    sf::Vector3i mappos;
    
    TileRenderer renderer;
    std::string key;

    TextureImpl textures;
};

#endif /* ISOMETRIC_TILE_HPP */
