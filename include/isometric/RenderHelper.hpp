/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ISOMETRIC_RENDERHELPER_HPP
#define ISOMETRIC_RENDERHELPER_HPP

#include "Coordinate.hpp"
#include "Entity.hpp"
#include "Tile.hpp"

#include <vector>
#include <map>

class RenderHelper : public sf::Drawable, public sf::Transformable
{
public:
    static int width;  // x
    static int height; // z
    static int depth;  // y
public:
    explicit RenderHelper(std::map<Coordinate, Tile>& mapTiles,
                          std::vector<Entity::Ptr>& mapEntities) noexcept;

    void update(const sf::Vector3i& center) noexcept;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const;
private:
    std::map<Coordinate, Tile>& mapDataTiles;
    std::vector<Entity::Ptr>& mapDataEntities;

    sf::Vector3i player;
};

#endif /* ISOMETRIC_RENDERHELPER_HPP */
