/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ISOMETRIC_ENTITY_HPP
#define ISOMETRIC_ENTITY_HPP

#include <SFML/Graphics/Sprite.hpp>

#include <SFML/System/Vector3.hpp>


#include <memory>

namespace sf
{
    class Time;
}

class EntityInformation;

class Entity : public sf::Drawable, public sf::Transformable
{
public:
    using Ptr = std::unique_ptr<Entity>;
public:
    explicit Entity(EntityInformation& info) noexcept;

    std::string name() const noexcept;

    EntityInformation& info() const noexcept;
    
    void setMapPosition(const sf::Vector3i& pos) noexcept;
    const sf::Vector3i& getMapPosition() const noexcept;

    int getZSlab() const noexcept;
    void increaseSlab() noexcept;
    void decreaseSlab() noexcept;

    void onMoveDone() noexcept;
    void onMoveFail() noexcept;

    void draw(sf::RenderTarget& target, sf::RenderStates states) const;
protected:
    void updatePos() noexcept;
private:
    sf::Vector3i mappos;
    int z_slab;
    EntityInformation* myInfo;

    sf::Sprite spr;
};

#endif /* ISOMETRIC_ENTITY_HPP */
