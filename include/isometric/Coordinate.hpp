/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ISOMETRIC_COORDINATE_HPP
#define ISOMETRIC_COORDINATE_HPP

#include <SFML/System/Vector3.hpp>

struct Coordinate
{
    explicit Coordinate() noexcept;
    Coordinate(int X, int Y, int Z) noexcept;

    Coordinate operator=(const sf::Vector3i& vec) const noexcept;

    int x;
    int y;
    int z;
};

bool operator<(const Coordinate& lhs, const Coordinate& rhs) noexcept;
bool operator==(const Coordinate& lhs, const Coordinate& rhs) noexcept;

#endif /* ISOMETRIC_COORDINATE_HPP */
