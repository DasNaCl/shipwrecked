/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ISOMETRIC_TILERENDERER_HPP
#define ISOMETRIC_TILERENDERER_HPP

#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Drawable.hpp>

#include <map>

class TextureImpl;
class TilesetDataType;

class TileRendererData
{
    friend class TileRenderer;
public:
    void set(const TilesetDataType& t) noexcept;
private:
    std::map<std::string, sf::VertexArray> vertexArrays;
};

class TileRenderer : public sf::Drawable
{
public:
    explicit TileRenderer(TileRendererData& rendererdata) noexcept;

    void setTextureImpl(TextureImpl& textureImpl) noexcept;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
private:
    mutable TileRendererData* data;
    TextureImpl* texImpl;
};

#endif /* ISOMETRIC_TILERENDERER_HPP */
