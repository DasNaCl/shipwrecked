/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ISOMETRIC_ENTITYUPDATER_HPP
#define ISOMETRIC_ENTITYUPDATER_HPP

#include "Coordinate.hpp"
#include "Entity.hpp"
#include "Tile.hpp"

#include <map>

class TileResource;
class ScriptInformation;

class EntityUpdater
{
public:
    explicit EntityUpdater(const std::map<Coordinate, Tile>& data,
                           const std::vector<Entity::Ptr>& entities,
                           const sf::Time& dt,
                           TileResource& tileResourceParam,
                           const ScriptInformation& input) noexcept;

private:
    void updateSlab(Entity& e) const noexcept;
    static void registerScripting() noexcept;
private:
    const std::map<Coordinate, Tile>& tiles;
    TileResource& tileResource;

    static bool scriptingRegistered;
};

#endif /* ISOMETRIC_ENTITYUPDATER_HPP */
