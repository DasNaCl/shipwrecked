/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ISOMETRIC_TEXTUREMANAGER_HPP
#define ISOMETRIC_TEXTUREMANAGER_HPP

#include "TextureKey.hpp"

#include <SFML/Graphics/Texture.hpp>

#include <functional>
#include <string>
#include <vector>
#include <map>

class TextureManager
{
public:
    explicit TextureManager() noexcept;
    explicit TextureManager(
        const std::vector<std::pair<TextureKey, std::string>>& list) noexcept;

    void add(TextureKey key, std::string path) noexcept;
    sf::Texture& get(TextureKey key);
private:
    std::map<TextureKey, std::function<bool(sf::Texture&)>> factories;
    std::map<TextureKey, sf::Texture> textures;
};

#endif /* ISOMETRIC_TEXTUREMANAGER_HPP */
