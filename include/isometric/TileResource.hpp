/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ISOMETRIC_TILERESOURCE_HPP
#define ISOMETRIC_TILERESOURCE_HPP

#include "TileInformation.hpp"

#include "angelscript/TextureImpl.hpp"
#include "isometric/TextureManager.hpp"

#include <SFML/System/Vector3.hpp>

#include <string>
#include <map>

class Entity;

class TileResource
{
public:
    explicit TileResource(std::string filepath = "");

    bool allowsMovement(const std::string& key, Entity& e,
                        const sf::Vector3i& to) const noexcept;

    const TextureImpl& defaultTextures(const std::string& key) const noexcept;

    int zSlabHeight(const std::string& key) const noexcept;
    MovementType movementType(const std::string& key) const noexcept;
private:
    const TileInformation& getTileInformation(const std::string& key) const noexcept;

    void loadStandard();
    void loadFromFile(const std::string& filepath);
private:
    void initScripting();
    
    void add(const std::string& tileid, unsigned int zheight,
             unsigned int movementType,
             const angelscript::TextureImpl& pim) noexcept;
public:
    void addRef(){}
    void releaseRef(){}
private:
    std::map<std::string, TileInformation> data;

    TextureManager texmanager;
};

#endif /* ISOMETRIC_TILERESOURCE_HPP */
