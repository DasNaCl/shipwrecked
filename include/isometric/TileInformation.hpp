/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ISOMETRIC_TILEINFORMATION_HPP
#define ISOMETRIC_TILEINFORMATION_HPP

#include "TextureImpl.hpp"

#include <SFML/System/Vector3.hpp>

#include <functional>
#include <vector>

class Entity;

enum class MovementType : unsigned int
{
    None = 0U,
    Normal = 1U << 0U,
    Lift = 1 << 1,
};

class TileInformation
{
public:
    std::string key;

    int zSlabHeight;
    MovementType movementType;

    TextureImpl defaultTextures;
    
    std::size_t openDirections;

    using FuncT = std::function<bool(Entity&, const sf::Vector3i&)>;
    std::vector<FuncT> rulesets;
};

#endif /* ISOMETRIC_TILEINFORMATION_HPP */
