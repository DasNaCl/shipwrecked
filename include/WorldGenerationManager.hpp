/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef WORLDGENERATIONMANAGER_HPP
#define WORLDGENERATIONMANAGER_HPP

#include "isometric/Coordinate.hpp"
#include "isometric/Tile.hpp"
#include "PerlinNoise.hpp"

#include <SFML/System/Vector2.hpp>
#include <SFML/System/Vector3.hpp>

#include <map>

class TilesetData;
class TileResource;

class WorldGenerationManager
{
public:
    struct Vec3
    {
        int x;
        int y;
        int z;
        
        void addRef() {}
        void rmRef() {}
    };
public:
    explicit WorldGenerationManager(const sf::Vector2u& mapsize,
                                    std::map<Coordinate, Tile>& mapdata,
                                    TilesetData& tilesetdata,
                                    TileResource& tileResource);
    sf::Vector3i spawnPoint() const noexcept;
private:
    void registerAPI() noexcept;
    void registerMathTrig() noexcept;
    void registerMathAntiTrig() noexcept;
    void registerMathFloating() noexcept;
    void registerMathCommon() noexcept;
    void registerPerlinNoise() noexcept;
private:
    void add(int x, int y, int z, const std::string& type) noexcept;
    void addWithOff(int x, int y, int z, const std::string& type,
                    int texOffX, int texOffY) noexcept;
    unsigned int width() const noexcept;
    unsigned int height() const noexcept;
public:
    void addRef(){}
    void releaseRef(){}
private:
    std::map<Coordinate, Tile>& map;
    TilesetData& tileset;
    sf::Vector2u size;
    TileResource& tileResource;

    Vec3 spawn;
    PerlinNoise perlinNoise;
};

#endif /* WORLDGENERATIONMANAGER_HPP */
