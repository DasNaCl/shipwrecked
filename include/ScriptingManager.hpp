/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SCRIPTINGMANAGER_HPP
#define SCRIPTINGMANAGER_HPP

#include <string>

class asIScriptFunction;
class asIScriptEngine;
class asIScriptModule;

class ScriptingManager
{
public:
    explicit ScriptingManager();
    ~ScriptingManager() noexcept;

    asIScriptEngine* engine() noexcept;
    asIScriptModule* module(const std::string& module) noexcept;

    void add(const std::string& module, const std::string& name,
             const std::string& script);
    void run(const std::string& module, const std::string& entry = "void main()");
private:
    asIScriptEngine* eng;
private:
    ScriptingManager(const ScriptingManager& mgr) noexcept = delete;
    ScriptingManager(ScriptingManager&& mgr) noexcept = delete;

    ScriptingManager& operator=(const ScriptingManager& mgr) noexcept = delete;
    ScriptingManager& operator=(ScriptingManager&& mgr) noexcept = delete;
};

static ScriptingManager scriptingManager;

#endif /* SCRIPTINGMANAGER_HPP */
