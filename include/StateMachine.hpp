/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef STATEMACHINE_HPP
#define STATEMACHINE_HPP

#include "State.hpp"

#include <functional>
#include <string>
#include <vector>
#include <map>

class StateMachine : public sf::Drawable,
                     public sf::Transformable,
                     public sf::NonCopyable
{
public:
    enum class Action
    { Push, Pop, Clear, };

    friend void State::requestClear();
    friend void State::requestPop();
    friend void State::requestPush(const std::string& id);
private:
    struct Pending
    {
        explicit Pending() noexcept = default;

        Action action;
        std::string id;
    };
public:
    explicit StateMachine() noexcept;

    void update(const sf::Time& dt);
    void handleEvent(const sf::Event& event);
    void handleMousePos(const sf::RenderTarget& target,
                        sf::Vector2i mouse);

    template<class T>
    void rememberState(const std::string& id) noexcept
    {
        factories[id] = [this]()->State::Ptr
        {
            return State::Ptr(new T(*this));
        };
    }
    template<class T, class... D>
    void rememberState(const std::string& id,
                       const std::tuple<D...>& args) noexcept
    {
        factories[id] = [this,args]()->State::Ptr
        {
            return State::Ptr(new T(*this, args));
        };
    }

    bool isEmpty() const noexcept;

    void push(const std::string& id);
private:
    void pop() noexcept;
    void clear() noexcept;

    void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    State::Ptr executeFactory(const std::string& id) const;
    void applyPendingChanges();
private:
    using Factory = std::function<State::Ptr()>;
private:
    std::vector<State::Ptr> stack;
    std::vector<Pending> pendingChanges;
    std::map<std::string, Factory> factories;
};


#endif /* STATEMACHINE_HPP */
