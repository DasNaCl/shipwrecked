/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef KEYWATCH_HPP
#define KEYWATCH_HPP

#include "ActionWatch.hpp"

#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Mouse.hpp>

#include <unordered_map>

class KeyWatch
{
public:
    explicit KeyWatch() noexcept;

    void update() noexcept;
    bool pressed(sf::Keyboard::Key key, bool doUpdate = true) noexcept;
    bool pressed(sf::Mouse::Button but, bool doUpdate = true) noexcept;

    bool released(sf::Keyboard::Key key, bool doUpdate = true) noexcept;
    bool released(sf::Mouse::Button but, bool doUpdate = true) noexcept;
private:
    std::unordered_map<int, ActionWatch> map;
};

static KeyWatch KeyWatcher;

#endif /* KEYWATCH_HPP */
