/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef TEXTUREIMPL_HPP
#define TEXTUREIMPL_HPP

#include <SFML/System/Vector2.hpp>

#include <string>
#include <map>

namespace sf
{
    class Texture;
}

namespace angelscript
{
    class TextureImpl;
}

class TextureImpl
{
public:
    explicit TextureImpl(sf::Texture& texture) noexcept;
    explicit TextureImpl(sf::Texture& texture,
                         const angelscript::TextureImpl& impl) noexcept;

    bool has(const std::string& side) const noexcept;
    void set(const std::string& side, const sf::Vector2f& off) noexcept;
    void set(const sf::Vector2f& off) noexcept;

    void clear() noexcept;
public:
    std::map<std::string, sf::Vector2f> map;
    sf::Texture* tex;
};

#endif /* TEXTUREIMPL_HPP */
