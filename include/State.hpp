/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef STATE_HPP
#define STATE_HPP

#include <SFML/Graphics/Transformable.hpp>
#include <SFML/Graphics/Drawable.hpp>

#include <SFML/System/NonCopyable.hpp>

#include <memory>

namespace sf
{
    class Time;
    class Event;
}

class StateMachine;

class State : public sf::Drawable,
              public sf::Transformable,
              public sf::NonCopyable
{
public:
    using Ptr = std::unique_ptr<State>;
public:
    enum class Strategy
    { Nothing, DoNotUpdateLower, };
            
public:
    explicit State(StateMachine& m) noexcept;
    virtual ~State() = default;

    virtual Strategy update(const sf::Time& dt) noexcept = 0;
    virtual Strategy handleEvent(const sf::Event& event) noexcept = 0;
    virtual Strategy handleMousePos(const sf::Vector2i& mouse,
                                    const sf::RenderTarget& target) noexcept = 0;
protected:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;
public:
    void requestPush(const std::string& id);
    void requestPop() noexcept;
    void requestClear() noexcept;
private:
    StateMachine& machine;
};

#endif /* STATE_HPP */
