/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef GAMESTATE_HPP
#define GAMESTATE_HPP

#include "State.hpp"
#include "GameStateGui.hpp"

#include "isometric/IsometricMap.hpp"
#include "isometric/Entity.hpp"

#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/View.hpp>

#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>

class GameState : public State
{
public:
    explicit GameState(StateMachine& m);

    State::Strategy update(const sf::Time& dt) noexcept;
    State::Strategy handleEvent(const sf::Event& event) noexcept;
    State::Strategy handleMousePos(const sf::Vector2i& mouse,
                                   const sf::RenderTarget& target) noexcept;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const;
private:
    struct MouseCoordinate
    {
        sf::Vector2i pixel;
        sf::Vector2i view;
        sf::Vector2i world;
    };
private:
    Entity* player;
    IsometricMap isomap;

    sf::View view;
    MouseCoordinate mousePosition;

    mutable sf::RenderTexture sfguiRenderTexture;
    mutable sf::RenderTexture gameviewRenderTexture;
    mutable GameStateGui gui;
};

#endif /* GAMESTATE_HPP */
