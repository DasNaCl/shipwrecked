/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef CONFIG_HPP
#define CONFIG_HPP

#include "isometric/TextureManager.hpp"

#include <string>
#include <vector>

struct Config
{
    struct Common
    {
        static std::string configFile;
        static std::string tileResourceFile;
        static std::string worldgenerationFile;
        static TextureManager texmanager;
    };
    
    struct Application
    {
        static std::string title;
        static std::size_t width;
        static std::size_t height;
        static std::size_t bits;
        static bool verticalSync;
        static unsigned int antialiasing;
    };

    struct World
    {
        static std::size_t size;
    };

    struct Entities
    {
        static std::string path;
    };
};

#endif /* CONFIG_HPP */
