/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ANIMATOR_HPP
#define ANIMATOR_HPP

#include <SFML/System/Time.hpp>

#include <type_traits>
#include <functional>
#include <stdexcept>
#include <string>
#include <cmath>
#include <map>

template<typename T, typename KeyT = std::string>
class Animator
{
public:
    static_assert(!(std::is_floating_point<KeyT>::value),
                  "KeyT must not be a floating point as != is used.");
public:
    using Functor = std::function<void(T&, float)>;
    using AnimationType = std::pair<Functor, sf::Time>;
public:
    explicit Animator() noexcept
        : animations(), currentAnimation(), progress(0.f), loop(false)
        {

        }

    void append(const KeyT& key,
                const Functor& functor,
                const sf::Time& duration) noexcept
        {
            animations[key] =  std::make_pair(functor, duration);
        }

    void play(const KeyT& key, bool looping = false) 
        {
            auto it = animations.find(key);
            if(it == animations.end())
                throw std::invalid_argument("Key is unknown!");
            currentAnimation = it;
            loop = looping;
        }
    void stop() noexcept
        {
            currentAnimation = animations.end();
            progress = 0.f;
        }
    
    void animate(T& val) noexcept
        {
            if(isPlaying())
                currentAnimation->second.first(val, progress);
        }
    void update(const sf::Time& dt) noexcept
        {
            if(!isPlaying())
                return;

            const sf::Time total = currentAnimation->second.second;
            progress += dt.asSeconds() / total.asSeconds();

            if(progress > 1.f)
            {
                if(loop)
                    progress = 0.f;
                else
                    stop();
            }
        }

    bool isPlaying() noexcept
        {
            return currentAnimation != animations.end();
        }
    KeyT getPlaying() noexcept
        {
            if(isPlaying())
                return currentAnimation->first;
            return KeyT();
        }
private:
    std::map<KeyT, AnimationType> animations;

    std::map<KeyT, AnimationType>::iterator currentAnimation;
    float progress;
    bool loop;
private:
    Animator(Animator&&) = delete;
    Animator(const Animator&) = delete;
    Animator& operator=(const Animator&) = delete;
    Animator& operator=(Animator&&) = delete;
};

#endif /* ANIMATOR_HPP */
