/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SCRIPTINFORMATION_HPP
#define SCRIPTINFORMATION_HPP

class ScriptInformation
{
public:
    enum class Direction : int
    {
        NONE = 0,
        LEFT = 1 << 0,
        RIGHT = 1 << 1,
        DOWN = 1 << 2,
        UP = 1 << 3,
    };
public:
    bool leftPressed() const noexcept;
    bool rightPressed() const noexcept;
    bool upPressed() const noexcept;
    bool downPressed() const noexcept;

    void set(Direction dir) noexcept;
public:
    void addRef() noexcept {}
    void rmRef() noexcept {}
private:
    Direction pressed_status {Direction::NONE};
};

#endif /* SCRIPTINFORMATION_HPP */
