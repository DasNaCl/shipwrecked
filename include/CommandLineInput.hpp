/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef COMMANDLINEINPUT_HPP
#define COMMANDLINEINPUT_HPP

#include <functional>
#include <stdexcept>
#include <ostream>
#include <sstream>
#include <memory>
#include <string>
#include <vector>

struct BasicParameter
{
public:
    BasicParameter(const std::string& helpText) noexcept;

    virtual void read(const std::string& toProcess) = 0;
    std::string helpText() const noexcept;

    virtual bool isVoid() const noexcept
    { return false; }

    bool doStop() const noexcept
    { return makeStop; }
private:
    std::string help;
protected:
    bool makeStop;
};

template<class T>
struct Parameter : public BasicParameter
{
public:
    Parameter(const std::string& helpText,
              const std::function<void(const T&)>& func) noexcept
        : BasicParameter(helpText), toCall(func)
    {  }

    const Parameter<T>& stopOnCall()
    { makeStop = true; return *this; }
private: 
    void read(const std::string& toProcess) override
    {
        if(toProcess.empty())
            throw std::invalid_argument("Missing parameter");
        std::stringstream ss(toProcess);
        T val;
        ss >> val;

        toCall(val);
    }
private:
    std::function<void(const T&)> toCall;
};

template<>
struct Parameter<void> : public BasicParameter
{
public:
    Parameter(const std::string& helpText,
              const std::function<void()>& func) noexcept
        : BasicParameter(helpText), toCall(func)
    {  }

    const Parameter<void>& stopOnCall()
    { makeStop = true; return *this; }

    bool isVoid() const noexcept override
    { return true; }
private:
    void read(const std::string& toProcess) override
    {
        toCall();
    }
private:
    std::function<void()> toCall;
};

struct BasicCommand
{
public:
    BasicCommand(const std::string& shortVersion,
                 const std::string& longVersion,
                 const std::string& description) noexcept;
public:
    std::string shortVer;
    std::string longVer;
    std::string desc;
    std::vector<std::unique_ptr<BasicParameter>> parameters;
};

struct ParameterAdder
{
public:
    ParameterAdder(BasicCommand& command) noexcept;

    template<class T>
    ParameterAdder operator()(const Parameter<T>& param)
    {
        cmd->parameters.emplace_back(std::make_unique<Parameter<T>>(param));

        return ParameterAdder(*cmd);
    }
private:
    BasicCommand* cmd;
};

class CommandLineInput
{
public:
    enum class Result
    {
        Ok = 0,
        Stopped = 1,
    };
public:
    CommandLineInput(const std::string& programTitle,
                     int argc, char* argv[]) noexcept;
    ParameterAdder add(const std::string& shortVersion,
                       const std::string& longVersion,
                       const std::string& description) noexcept;

    Result read();
    
    friend std::ostream& operator<<(std::ostream& os, const CommandLineInput& in);
private:
    void printHelp(std::ostream& os) const noexcept;
private:
    std::string title;
    std::vector<BasicCommand> commands;

    std::vector<std::string> arguments;
};

#endif /* COMMANDLINEINPUT_HPP */
