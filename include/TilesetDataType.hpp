/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef TILESETDATATYPE_HPP
#define TILESETDATATYPE_HPP

#include "isometric/TileRenderer.hpp"

#include <SFML/System/Vector2.hpp>

#include <string>
#include <vector>
#include <map>

class TilesetData;

struct TilesetDataType
{
    explicit TilesetDataType(const std::string& tile)
        : ref(), sides(), tileRendererData(), tileID(tile)
    {  }

    inline std::vector<sf::Vector2i>& operator[](const std::string& side) noexcept
    { return sides[side]; }

    inline TileRenderer renderer() noexcept
    { return TileRenderer(tileRendererData); }

    sf::Vector2i ref;
    std::map<std::string, std::vector<sf::Vector2i>> sides;

    TileRendererData tileRendererData;

private:
    std::string tileID;
    
};

#endif /* TILESETDATATYPE_HPP */
