/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef GAMESTATEGUI_HPP
#define GAMESTATEGUI_HPP

#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>

#include <string>

namespace sf
{
    class Time;
}

class GameStateGui
{
public:
    struct InformationBar
    {
    public:
        explicit InformationBar(const std::string& name) noexcept;

        sfg::Frame::Ptr& get() noexcept
        { return frame; }
        sfg::Scale::Ptr& getScale() noexcept
        { return scale; }
    private:
        sfg::Frame::Ptr frame;
        sfg::Scale::Ptr scale;
        sfg::Box::Ptr box;
    };
public:
    explicit GameStateGui() noexcept;

    void update(const sf::Time& dt) noexcept;
    void draw(sf::RenderTexture& canvas) noexcept;
    void handleEvent(const sf::Event& event) noexcept;
private:
    sfg::SFGUI sfgui;
    sfg::Window::Ptr window;
    sfg::Box::Ptr box;
    InformationBar health;
    InformationBar thirst;
    InformationBar hunger;
    sfg::Button::Ptr button;
};

#endif /* GAMESTATEGUI_HPP */
