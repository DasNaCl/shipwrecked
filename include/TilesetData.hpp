/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef TILESETDATA_HPP
#define TILESETDATA_HPP

#include "TilesetDataType.hpp"

class TilesetData
{
public:
    void add(const std::string& type) noexcept;
    void addSide(const std::string& type, const std::string& side) noexcept;

    TileRenderer renderer(const std::string& type) noexcept;
    void updateRenderer(const std::string& type) noexcept;

    std::vector<std::string> tileTypeSides(const std::string& type) noexcept;
    
    bool hasTileTypes() const noexcept;
    std::vector<std::string> tileTypes() const noexcept;
    TilesetDataType& operator[](const std::string& type) noexcept;
    const TilesetDataType& operator[](const std::string& type) const noexcept;
private:
    std::map<std::string, TilesetDataType> data;
};

#endif /* TILESETDATA_HPP */
