/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include <string>
#include <vector>

enum class ApplicationResult : int
{
    Normal = 0,
    NotNormal = 1,
    Redo = 2,
};

class Application
{
public:
    explicit Application() noexcept;

    ApplicationResult run();
};

#endif /* APPLICATION_HPP */
