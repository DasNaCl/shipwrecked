/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ACTIONWATCH_HPP
#define ACTIONWATCH_HPP

#include <functional>

class ActionWatch
{
public:
    explicit ActionWatch(std::function<bool()> func,
                         bool prev = false,
                         bool curr = false) noexcept;
    void update() noexcept;
    void setFunc(std::function<bool()> func) noexcept;

    bool isActive(bool doUpdate = true) noexcept;
    bool released(bool doUpdate = true) noexcept;
private:
    std::function<bool()> f;
    bool v0;
    bool v1;
};

#endif /* ACTIONWATCH_HPP */
