void main()
{
    if(information.leftPressed())
        entity.moveX(-1);
    else if(information.rightPressed())
        entity.moveX(1);

    if(information.upPressed())
        entity.moveY(-1);
    else if(information.downPressed())
        entity.moveY(1);
}
