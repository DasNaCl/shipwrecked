#!/bin/bash

source="$(find src/* | grep -v "~" | grep -v "#" | grep ".cpp" | tr '\n' ' ')"
extsource=""
link="-I include/ -L lib/ -lpthread -langelscript -lsfml-graphics -lsfml-window -lsfml-system -lsfgui"
defines=""

function debug
{
    mkdir -p bin
    mkdir -p bin/Debug
    g++ -ggdb -std=c++1z $defines $source $extsource $link
    mv a.out bin/Debug/a1
}

function release
{
    mkdir -p bin
    mkdir -p bin/Release
    g++ -O2 -s -std=c++1z $defines $source $extsource $link
    mv a.out bin/Release/a1
}

dr=$1
if [ -z "$dr" ] ; then
    read -p "Compile for [Dd]ebug or [Rr]elease? (q to quit) : " dr
fi
while true; do
    case $dr in
    [Dd]* ) debug; break;;
    [Rr]* ) release; break;;
     [q]* ) exit;;
        * ) echo "Please provide an valid answer, either d or r!";;
    esac
    read -p "Compile for [Dd]ebug or [Rr]elease? (q to quit) : " dr
done
