/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "TilesetReader.hpp"
#include "TilesetData.hpp"
#include "json/json.h"

#include <stdexcept>
#include <fstream>

TilesetReader::TilesetReader(TilesetData& data, const std::string& filename)
{
    if(!data.hasTileTypes())
    {
        std::string error("TilesetReader: The provided instance of");
        error += "TilesetData needs at least on tile type!";

        throw std::invalid_argument(error);
    }
    
    Json::Value value;

    {
    std::string fileContent;
    std::ifstream fileInputStream(filename);
    if(fileInputStream.bad())
    {
        std::string error("TilesetReader: Your \"" + filename + "\" ");
        error += "is not accessible!";

        throw std::logic_error(error);
    }
    for(std::string s; std::getline(fileInputStream, s); fileContent += s)
    {  }

    Json::Reader reader;
    if(!reader.parse(fileContent, value))
    {
        std::string error("TilesetReader: \"" + filename + "\" ");
        error += "has one or more problems: " + reader.getFormattedErrorMessages();

        throw std::logic_error(error);
    }
    }

    std::string error;
    
    std::vector<std::string> types = data.tileTypes();
    for(const auto& type : types)
    {
        if(!value[type])
        {
            error += "TilesetReader: Tile type \"" + type + "\"";
            error += " is not present in \"" + filename + "\"!\n";
            continue;
        }
        Json::Value jtype = value[type];
            
        if(!jtype["REF"])
        {
            error += "TilesetReader: Tile type \"" + type + "\" from ";
            error += " file \"" + filename + "\" has no \"REF\" object!\n";
            continue;
        }
        Json::Value jref = jtype["REF"];

        TilesetDataType& tdt = data[type];
        
        tdt.ref.x = jref["x"].asInt();
        tdt.ref.y = jref["y"].asInt();

        std::vector<std::string> sides = data.tileTypeSides(type);
        for(const auto& side : sides)
        {
            if(!jtype[side])
            {
                error += "TilesetReader: The side \"" + side + "\" of ";
                error += "Tile type \"" + type + "\" is not present in ";
                error += "file \"" + filename + "\"!\n";
                continue;
            }
            Json::Value jside = jtype[side];

            std::vector<sf::Vector2i>& ref = tdt[side];
            ref.resize(jside.size());
            for(Json::ArrayIndex i = 0U; i < jside.size(); ++i)
            {
                ref[i].x = jside[i]["x"].asInt();
                ref[i].y = jside[i]["y"].asInt();
            }
        }
    }

    if(data.hasTileTypes())
    {
        for(const auto& d : data.tileTypes())
            data.updateRenderer(d);
    }

    if(!error.empty())
        throw std::logic_error(error);
}
