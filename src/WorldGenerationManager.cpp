/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "WorldGenerationManager.hpp"
#include "ScriptingManager.hpp"
#include "TilesetData.hpp"
#include "Config.hpp"

#include "angelscript/angelscript.hpp"

#include "isometric/TileResource.hpp"

#include <cassert>
#include <fstream>
#include <cmath>

WorldGenerationManager::WorldGenerationManager(const sf::Vector2u& mapsize,
                                               std::map<Coordinate, Tile>& mapdata,
                                               TilesetData& tilesetdata,
                                               TileResource& tileResourceParam)
: map(mapdata), tileset(tilesetdata),
  size(mapsize), tileResource(tileResourceParam), spawn()
{
    registerAPI();
    registerMathTrig();
    registerMathAntiTrig();
    registerMathFloating();
    registerMathCommon();
    registerPerlinNoise();

    std::fstream scriptfile(Config::Common::worldgenerationFile, std::ios::in);

    std::string script;
    for(std::string s; std::getline(scriptfile, s); script += s)
    {  }

    scriptingManager.add("worldgeneration", "generation", script.c_str());
    scriptingManager.run("worldgeneration");
}

sf::Vector3i WorldGenerationManager::spawnPoint() const noexcept
{
    return sf::Vector3i(spawn.x, spawn.y, spawn.z);
}

void WorldGenerationManager::registerAPI() noexcept
{
    auto* engine = scriptingManager.engine();
    int result = engine->RegisterObjectType("WorldGenerator", 0, asOBJ_REF);
    assert(result >= 0);
    result = engine->RegisterObjectBehaviour("WorldGenerator", asBEHAVE_ADDREF,
                                             "void addRef()",
                                             asMETHOD(WorldGenerationManager,
                                                      addRef),
                                             asCALL_THISCALL);
    assert(result >= 0);
    result = engine->RegisterObjectBehaviour("WorldGenerator", asBEHAVE_RELEASE,
                                             "void releaseRef()",
                                             asMETHOD(WorldGenerationManager,
                                                      releaseRef),
                                             asCALL_THISCALL);
    result = engine->RegisterGlobalProperty("WorldGenerator generator", this);
    assert(result >= 0);
    result = engine->RegisterObjectMethod("WorldGenerator",
                                          "void add(int,int,int,string)",
                                          asMETHOD(WorldGenerationManager, add),
                                          asCALL_THISCALL);
    assert(result >= 0);
    result = engine->RegisterObjectMethod("WorldGenerator",
                                   "void addWithOff(int,int,int,string,int,int)",
                                   asMETHOD(WorldGenerationManager, addWithOff),
                                   asCALL_THISCALL);
    assert(result >= 0);
    result = engine->RegisterObjectMethod("WorldGenerator",
                                          "uint width()",
                                          asMETHOD(WorldGenerationManager, width),
                                          asCALL_THISCALL);
    assert(result >= 0);
    result = engine->RegisterObjectMethod("WorldGenerator",
                                          "uint height()",
                                          asMETHOD(WorldGenerationManager, height),
                                          asCALL_THISCALL);
    assert(result >= 0);


    result = engine->RegisterObjectType("Vec3", 0, asOBJ_REF);
    assert(result >= 0);
    result = engine->RegisterObjectBehaviour("Vec3", asBEHAVE_ADDREF,
                                             "void addRef()",
                                             asMETHOD(Vec3, addRef),
                                             asCALL_THISCALL);
    assert(result >= 0);
    result = engine->RegisterObjectBehaviour("Vec3", asBEHAVE_RELEASE,
                                             "void rmRef()",
                                             asMETHOD(Vec3, rmRef),
                                             asCALL_THISCALL);
    assert(result >= 0);
    result = engine->RegisterObjectProperty("Vec3", "int x", asOFFSET(Vec3, x));
    assert(result >= 0);
    result = engine->RegisterObjectProperty("Vec3", "int y", asOFFSET(Vec3, y));
    assert(result >= 0);
    result = engine->RegisterObjectProperty("Vec3", "int z", asOFFSET(Vec3, z));
    assert(result >= 0);
    result = engine->RegisterGlobalProperty("Vec3 spawn", &spawn);
    assert(result >= 0);
}

void WorldGenerationManager::registerMathTrig() noexcept
{
    auto* engine = scriptingManager.engine();
    int result = engine->RegisterGlobalFunction("float sin(float rad)",
                                                asFUNCTION(sin),
                                                asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float cos(float rad)",
                                            asFUNCTION(cos),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float tan(float val)",
                                            asFUNCTION(tan),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float hypot(float x, float y)",
                                            asFUNCTION(hypot),
                                            asCALL_CDECL);
    assert(result >= 0);
}

void WorldGenerationManager::registerMathAntiTrig() noexcept
{
    auto* engine = scriptingManager.engine();
    int result = engine->RegisterGlobalFunction("float atan2(float x, float y)",
                                                asFUNCTION(atan2),
                                                asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float atan(float val)",
                                            asFUNCTION(atan),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float asin(float val)",
                                            asFUNCTION(asin),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float acos(float val)",
                                            asFUNCTION(acos),
                                            asCALL_CDECL);
    assert(result >= 0);
}

void WorldGenerationManager::registerMathFloating() noexcept
{
    auto* engine = scriptingManager.engine();
    int result = engine->RegisterGlobalFunction("float ceil(float val)",
                                            asFUNCTION(ceil),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float floor(float val)",
                                            asFUNCTION(floor),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float trunc(float val)",
                                            asFUNCTION(trunc),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float round(float val)",
                                            asFUNCTION(round),
                                            asCALL_CDECL);
    assert(result >= 0);
}

void WorldGenerationManager::registerMathCommon() noexcept
{
    auto* engine = scriptingManager.engine();
    int result = engine->RegisterGlobalFunction("int abs(int val)",
                                            asFUNCTION(abs),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float fabs(float value)",
                                            asFUNCTION(fabs),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float exp(float exponent)",
                                            asFUNCTION(exp),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float exp2(float exponent)",
                                            asFUNCTION(exp2),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float log(float val)",
                                            asFUNCTION(log),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float log2(float val)",
                                            asFUNCTION(log2),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float log10(float val)",
                                            asFUNCTION(log10),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float pow(float val)",
                                            asFUNCTION(pow),
                                            asCALL_CDECL);
    assert(result >= 0);
    result = engine->RegisterGlobalFunction("float sqrt(float val)",
                                            asFUNCTION(sqrt),
                                            asCALL_CDECL);
    assert(result >= 0);
}

void WorldGenerationManager::registerPerlinNoise() noexcept
{
    auto* engine = scriptingManager.engine();
    int result = engine->RegisterObjectType("PerlinNoise", 0, asOBJ_REF);
    assert(result >= 0);
    result = engine->RegisterObjectBehaviour("PerlinNoise", asBEHAVE_ADDREF,
                                             "void addRef()",
                                             asMETHOD(PerlinNoise, addRef),
                                             asCALL_THISCALL);
    assert(result >= 0);
    result = engine->RegisterObjectBehaviour("PerlinNoise", asBEHAVE_RELEASE,
                                             "void rmRef()",
                                             asMETHOD(PerlinNoise, rmRef),
                                             asCALL_THISCALL);
    assert(result >= 0);
    result = engine->RegisterObjectMethod("PerlinNoise",
                                          "float get(uint x, uint y)",
                                          asMETHOD(PerlinNoise, get),
                                          asCALL_THISCALL);
    assert(result >= 0);
    result = engine->RegisterGlobalProperty("PerlinNoise noise", &perlinNoise);
    assert(result >= 0);
}

void WorldGenerationManager::add(int x, int y, int z,
                                 const std::string& type) noexcept
{
    map.emplace(std::piecewise_construct,
                std::forward_as_tuple(x, y, z),
                std::forward_as_tuple(type, tileset.renderer(type),
                                      tileResource.defaultTextures(type)));
    auto it = map.find({x, y, z});
    assert(it != map.end());
    it->second.setMapPosition({x, y, z});    
}

void WorldGenerationManager::addWithOff(int x, int y, int z,
                                        const std::string& type,
                                        int texOffX, int texOffY) noexcept
{
    map.emplace(std::piecewise_construct,
                std::forward_as_tuple(x, y, z),
                std::forward_as_tuple(type, tileset.renderer(type),
                                      tileResource.defaultTextures(type)));
    auto it = map.find({x, y, z});
    assert(it != map.end());
    it->second.setMapPosition({x, y, z});
    it->second.setOffset(sf::Vector2f(texOffX, texOffY));
}

unsigned int WorldGenerationManager::width() const noexcept
{
    return size.x;
}

unsigned int WorldGenerationManager::height() const noexcept
{
    return size.y;
}
