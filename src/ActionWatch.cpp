/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "ActionWatch.hpp"

ActionWatch::ActionWatch(std::function<bool()> func,
                         bool prev, bool curr) noexcept
: f(func), v0(prev), v1(curr)
{

}

void ActionWatch::update() noexcept
{
    v1 = v0;
    v0 = f();
}

void ActionWatch::setFunc(std::function<bool()> func) noexcept
{
    f = func;
}

bool ActionWatch::isActive(bool doUpdate) noexcept
{
    if(doUpdate)
        update();

    return v0;
}

bool ActionWatch::released(bool doUpdate) noexcept
{
    if(doUpdate)
        update();

    return (!v0 && v1);
}
