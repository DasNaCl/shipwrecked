/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "KeyWatch.hpp"

#include <cassert>

KeyWatch::KeyWatch() noexcept
: map()
{
    int oldges = 0;
    int ges = static_cast<int>(sf::Keyboard::KeyCount);

    using FuncT = std::function<bool()>;
    for(int j = 0; j < 2; ++j)
    {
        for(int i = oldges; i < ges; ++i)
        {
            FuncT keyboardfunc = std::bind(sf::Keyboard::isKeyPressed,
                                           static_cast<sf::Keyboard::Key>(i));
            FuncT mousebutfunc = std::bind(sf::Mouse::isButtonPressed,
                                           static_cast<sf::Mouse::Button>(i - oldges));
            if(j == 0)
                map.emplace(i, ActionWatch(keyboardfunc));
            else if(j == 1)
                map.emplace(i, ActionWatch(mousebutfunc));
        }

        oldges = ges;

        if(j == 0)
            ges += static_cast<int>(sf::Mouse::ButtonCount);
    }
}

void KeyWatch::update() noexcept
{
    for(auto& w : map)
        w.second.update();
}

bool KeyWatch::pressed(sf::Keyboard::Key key, bool doUpdate) noexcept
{
    if(doUpdate)
        update();

    auto it = map.find(static_cast<int>(key));
    assert(it != map.end());

    return it->second.isActive(false);
}

bool KeyWatch::pressed(sf::Mouse::Button but, bool doUpdate) noexcept
{
    if(doUpdate)
        update();

    const int offset = static_cast<int>(sf::Keyboard::KeyCount);

    auto it = map.find(offset + static_cast<int>(but));
    assert(it != map.end());

    return it->second.isActive(false);
}

bool KeyWatch::released(sf::Keyboard::Key key, bool doUpdate) noexcept
{
    if(doUpdate)
        update();

    auto it = map.find(static_cast<int>(key));
    assert(it != map.end());

    return it->second.released(false);
}

bool KeyWatch::released(sf::Mouse::Button but, bool doUpdate) noexcept
{
    if(doUpdate)
        update();

    const int offset = static_cast<int>(sf::Keyboard::KeyCount);

    auto it = map.find(offset + static_cast<int>(but));
    assert(it != map.end());

    return it->second.released(false);
}
