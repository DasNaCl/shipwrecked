/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "Application.hpp"

#include "CommandLineInput.hpp"
#include "Config.hpp"

#include <iostream>

int main(int argc, char* argv[])
{
    CommandLineInput cmd("Shipwrecked", argc, argv);
    cmd.add("-h", "--help", "Prints this help.")
        (Parameter<void>("", [&cmd]()
                         {
                             std::cout << cmd << std::endl;
                         }).stopOnCall());
    cmd.add("-c", "--config", "Set config file path.")
        (Parameter<std::string>("valid path", [](const std::string& path)
                                {
                                    Config::Common::configFile = path;
                                }));
    cmd.add("-wi", "--width", "Set application\'s width.")
        (Parameter<std::size_t>("positive integer", [](std::size_t arg)
                                {
                                    Config::Application::width = arg;
                                }));
    cmd.add("-he", "--height", "Set application\'s height.")
        (Parameter<std::size_t>("positive integer", [](std::size_t arg)
                                {
                                    Config::Application::height = arg;
                                }));
    cmd.add("-bpp", "--bits-per-pixel", "Set bits per pixel.")
        (Parameter<std::size_t>("positive integer", [](std::size_t arg)
                                {
                                    Config::Application::bits = arg;
                                }));
    cmd.add("-ve", "--vertical-sync", "Enable vertical sync.")
        (Parameter<void>("", []()
                         {
                             Config::Application::verticalSync = true;
                         }));

    if(cmd.read() == CommandLineInput::Result::Stopped)
        return 0;

    ApplicationResult returnValue = ApplicationResult::Normal;
    do
    {
        try
        {
            Application app;
            returnValue = app.run();
        }
        catch(std::exception& e)
        {
            std::cout << e.what() << std::endl;
            
            returnValue = ApplicationResult::NotNormal;
        }
    } while(returnValue == ApplicationResult::Redo);

    return static_cast<int>(returnValue);
}
