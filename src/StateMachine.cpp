/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "StateMachine.hpp"

#include <SFML/Graphics/RenderTarget.hpp>

#include <stdexcept>
#include <cassert>

StateMachine::StateMachine() noexcept
: stack(), pendingChanges(), factories()
{

}

void StateMachine::update(const sf::Time& dt)
{
    for(auto& a : stack)
        if(a->update(dt) == State::Strategy::DoNotUpdateLower)
            break;

    applyPendingChanges();
}

void StateMachine::handleEvent(const sf::Event& event)
{
    for(auto& a : stack)
        if(a->handleEvent(event) == State::Strategy::DoNotUpdateLower)
            break;

    applyPendingChanges();
}

void StateMachine::handleMousePos(const sf::RenderTarget& target,
                                  sf::Vector2i mouse)
{
    for(auto& a : stack)
        if(a->handleMousePos(mouse, target) == State::Strategy::DoNotUpdateLower)
            break;

    applyPendingChanges();
}

bool StateMachine::isEmpty() const noexcept
{
    return stack.empty();
}

void StateMachine::push(const std::string& id)
{
    if(factories.find(id) == factories.end())
        throw std::invalid_argument("State \"" + id + "\" is not registered!");
    pendingChanges.emplace_back(Pending{Action::Push, id});
}

void StateMachine::pop() noexcept
{
    pendingChanges.emplace_back(Pending{Action::Pop, ""});
}

void StateMachine::clear() noexcept
{
    pendingChanges.emplace_back(Pending{Action::Clear, ""});
}

void StateMachine::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    for(const auto& state : stack)
        target.draw(*state, states);
}

State::Ptr StateMachine::executeFactory(const std::string& id) const
{
    auto it = factories.find(id);
    assert(it != factories.end());

    return it->second();
}

void StateMachine::applyPendingChanges()
{
    for(auto& p : pendingChanges)
    {
        switch(p.action)
        {
        default:
        case Action::Clear: stack.clear(); break;
        case Action::Push: stack.push_back(executeFactory(p.id)); break;
        case Action::Pop: stack.pop_back(); break;
        }
    }

    pendingChanges.clear();
}
