/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "CommandLineInput.hpp"

#include <algorithm>

BasicParameter::BasicParameter(const std::string& helpText) noexcept
: help(helpText), makeStop(false)
{

}

std::string BasicParameter::helpText() const noexcept
{
    return help;
}

BasicCommand::BasicCommand(const std::string& shortVersion,
                           const std::string& longVersion,
                           const std::string& description) noexcept
: shortVer(shortVersion), longVer(longVersion), desc(description), parameters()
{  }

ParameterAdder::ParameterAdder(BasicCommand& command) noexcept
: cmd(std::addressof(command))
{
    
}

CommandLineInput::CommandLineInput(const std::string& programTitle,
                                   int argc, char* argv[]) noexcept
: title(programTitle), commands(), arguments()
{
    arguments.resize(argc - 1);

    for(std::size_t i = 0U; i < argc - 1; ++i)
        arguments[i] = argv[i + 1U];
}

ParameterAdder CommandLineInput::add(const std::string& shortVersion,
                                     const std::string& longVersion,
                                     const std::string& description) noexcept
{
    commands.emplace_back(shortVersion, longVersion, description);

    return ParameterAdder(commands.back());
}

CommandLineInput::Result CommandLineInput::read()
{
    std::vector<std::string> cpy = arguments;

    for(auto it = cpy.begin(); it != cpy.end(); )
    {
        auto fit = std::find_if(commands.begin(), commands.end(),
                                [it](BasicCommand& command)
                                {
                                    return (*it == command.shortVer
                                         || *it == command.longVer);
                                });
        if(fit == commands.end())
            throw std::invalid_argument("\"" + *it + "\" is not known.");
        it = cpy.erase(it);

        for(auto& par : fit->parameters)
        {
            std::string nextArg;
            if(!par->isVoid())
            {
                if(it != cpy.end())
                {
                    nextArg = *it;
                    it = cpy.erase(it);
                }
            }
            par->read(nextArg);

            if(par->doStop())
                return Result::Stopped;
        }
    }

    return Result::Ok;
}

std::ostream& operator<<(std::ostream& os, const CommandLineInput& in)
{
    in.printHelp(os);

    return os;
}

void CommandLineInput::printHelp(std::ostream& os) const noexcept
{
    os << title;
    if(commands.empty())
        return;
    
    os << "args" << std::endl;
    os << "You can use the following arguments:\n";
    for(const auto& c : commands)
    {
        os << c.shortVer << " " << c.longVer << "\t";
        for(const auto& p : c.parameters)
            os << p->helpText() << "\t";
        os << c.desc << std::endl;
    }
}
