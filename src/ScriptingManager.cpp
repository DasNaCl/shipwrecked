/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "ScriptingManager.hpp"
#include "angelscript/angelscript.hpp"
#include "angelscript/scriptbuilder.hpp"
#include "angelscript/scriptstdstring.hpp"
#include "angelscript/scriptarray.hpp"

#include <stdexcept>
#include <iostream>
#include <cassert>

void MessageCallback(const asSMessageInfo *msg, void *param)
{
    const char *type = "ERR ";
    if(msg->type == asMSGTYPE_WARNING ) 
        type = "WARN";
    else if( msg->type == asMSGTYPE_INFORMATION ) 
        type = "INFO";
    std::clog << msg->section << " (" << msg->row << ", ";
    std::clog << msg->col << ") [" << type << "] : " << msg->message;
    std::clog << std::endl;
}

ScriptingManager::ScriptingManager()
: eng(asCreateScriptEngine())
{
    int result = eng->SetMessageCallback(asFUNCTION(MessageCallback),
                                         0, asCALL_CDECL);
    assert(result >= 0);
    RegisterStdString(eng);
    RegisterScriptArray(eng, true);
}

ScriptingManager::~ScriptingManager() noexcept
{
    if(eng)
        eng->ShutDownAndRelease();
}

asIScriptEngine* ScriptingManager::engine() noexcept
{
    return eng;
}

asIScriptModule* ScriptingManager::module(const std::string& module) noexcept
{
    return eng->GetModule(module.c_str());
}

void ScriptingManager::add(const std::string& module, const std::string& name,
                           const std::string& script)
{
    CScriptBuilder builder;
    int result = builder.StartNewModule(eng, module.c_str());
    if(result < 0)
        throw std::runtime_error("Module could not be started.");
    result = builder.AddSectionFromMemory(name.c_str(), script.c_str());
    if(result < 0)
    {
        std::string error("Script \"" + script + "\" has errors or is");
        error += "not accessable!";
        throw std::runtime_error(error);
    }
    result = builder.BuildModule();
    if(result < 0)
        throw std::runtime_error("Script \"" + script + "\" has errors!");
}

void ScriptingManager::run(const std::string& module, const std::string& entry)
{
    asIScriptModule* mod = eng->GetModule(module.c_str());
    asIScriptFunction* func = mod->GetFunctionByDecl(entry.c_str());
    if(!func)
    {
        std::string error("ScriptingManager: The script does not have ");
        error += "the function \"" + entry + "\"!";
        throw std::runtime_error(error);
    }

    asIScriptContext* ctx = eng->CreateContext();
    ctx->Prepare(func);
    
    int result = ctx->Execute();
    if(result != asEXECUTION_FINISHED)
    {
        if(result == asEXECUTION_EXCEPTION)
        {
            std::string error = ctx->GetExceptionString();
            ctx->Release();

            throw std::runtime_error(error);
        }
        else
        {
            ctx->Release();
            throw std::runtime_error("An error occured");
        }
    }
    ctx->Release();
}
