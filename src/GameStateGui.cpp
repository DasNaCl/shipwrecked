/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "GameStateGui.hpp"

#include "Config.hpp"

#include <SFML/System/Time.hpp>

GameStateGui::InformationBar::InformationBar(const std::string& name) noexcept
: frame(sfg::Frame::Create(name)),
  scale(sfg::Scale::Create(sfg::Scale::Orientation::HORIZONTAL)),
  box(sfg::Box::Create(sfg::Box::Orientation::VERTICAL))
{
    const std::size_t width = Config::Application::width;
    const std::size_t height = Config::Application::height;

    scale->GetAdjustment()->SetLower(0.f);
    scale->GetAdjustment()->SetUpper(1.f);
    scale->SetRequisition(sf::Vector2f(width * 0.08f, 0.04f * height));
    box->Pack(scale, false, false);

    frame->SetAlignment(sf::Vector2f(0.8f, 0.5f));
    frame->Add(box);
}

GameStateGui::GameStateGui() noexcept
: sfgui(), window(sfg::Window::Create()),
  box(sfg::Box::Create(sfg::Box::Orientation::VERTICAL)),
  health("Health"), thirst("Thirst"), hunger("Hunger"),
    button(sfg::Button::Create("Click"))
{
    const std::size_t width = Config::Application::width;
    const std::size_t height = Config::Application::height;

    window->SetStyle(window->GetStyle()
                     ^ sfg::Window::TITLEBAR ^ sfg::Window::RESIZE);
    window->SetRequisition(sf::Vector2f(width * 0.2f, height));

    box->SetRequisition(sf::Vector2f(width * 0.15f, 0.05f * height));
    box->Pack(health.get()); box->Pack(thirst.get()); box->Pack(hunger.get());
    box->Pack(button);
    window->Add(box);

    window->Update(0.f);
}

void GameStateGui::update(const sf::Time& dt) noexcept
{
    window->Update(dt.asSeconds());
}

void GameStateGui::draw(sf::RenderTexture& canvas) noexcept
{
    sfgui.Display(canvas);
}

void GameStateGui::handleEvent(const sf::Event& event) noexcept
{
    window->HandleEvent(event);
}
