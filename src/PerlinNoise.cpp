/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "PerlinNoise.hpp"
#include "Config.hpp"
#include "Random.hpp"

#include <functional>
#include <cassert>

PerlinNoise::PerlinNoise() noexcept
: noise()
{
    gen();
}

void PerlinNoise::gen() noexcept
{
    const float persistance = 0.5f;
    const float octaveCount = 5U;
    const std::size_t totalSize = Config::World::size * Config::World::size;
    auto whiteNoise = genWhiteNoise();

    noise.resize(totalSize);

    std::vector<std::vector<float>> smoothedNoises;
    smoothedNoises.resize(octaveCount);
    for(std::size_t i = 0U; i < octaveCount; ++i)
        smoothedNoises[i] = genSmoothNoise(whiteNoise, i);

    float amplitude = 1.f;
    float totalAmplitude = 0.f;
    for(std::size_t i = octaveCount - 1U; i >= 0; --i)
    {
        if(i == static_cast<std::size_t>(-1))
            break;
        amplitude *= persistance;
        totalAmplitude += amplitude;
        
        for(std::size_t k = 0U; k < totalSize; ++k)
            noise[k] += (amplitude * smoothedNoises[i][k]);
    }

    for(auto& val : noise)
        val /= totalAmplitude;
}

float PerlinNoise::get(std::size_t x, std::size_t y) const noexcept
{
    assert(x < Config::World::size && y < Config::World::size);

    return noise[x + y * Config::World::size];
}

std::vector<float> PerlinNoise::genWhiteNoise() noexcept
{
    const std::size_t total = Config::World::size * Config::World::size;
    std::vector<float> whiteNoise;
    whiteNoise.resize(total);

    std::uniform_real_distribution<float> dist(0.f, 1.f);
    auto roll = std::bind(dist, randomizer.gen());
    for(std::size_t i = 0U; i < total; ++i)
        whiteNoise[i] = roll();

    return whiteNoise;
}

std::vector<float> PerlinNoise::genSmoothNoise(const std::vector<float>& whiteNoise,
                                               std::size_t octave) const noexcept
{
    const std::size_t samplePeriod = 1U << octave;
    const float frequency = 1.f / samplePeriod;

    const std::size_t size = Config::World::size;
    
    std::vector<float> smoothedNoise;
    smoothedNoise.resize(size * size);
    for(std::size_t y = 0U; y < size; ++y)
    {
        std::size_t sample_y0 = (y / samplePeriod) * samplePeriod;
        std::size_t sample_y1 = (sample_y0 + samplePeriod) % size;
        float y_blend = (y - sample_y0) * frequency;

        for(std::size_t x = 0U; x < size; ++x)
        {
            std::size_t sample_x0 = (x / samplePeriod) * samplePeriod;
            std::size_t sample_x1 = (sample_x0 + samplePeriod) % size;
            float x_blend = (x - sample_x0) * frequency;

            float top = interpolate(whiteNoise[sample_x0 + sample_y0 * size],
                                    whiteNoise[sample_x1 + sample_y0 * size],
                                    y_blend);
            float bottom = interpolate(whiteNoise[sample_x0 + sample_y1 * size],
                                       whiteNoise[sample_x1 + sample_y1 * size],
                                       y_blend);
            smoothedNoise[x + y * size] = interpolate(top, bottom, x_blend);
        }
    }

    return smoothedNoise;
}
