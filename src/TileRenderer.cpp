/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "isometric/TileRenderer.hpp"
#include "isometric/Tile.hpp"

#include "TilesetDataType.hpp"

#include <SFML/Graphics/RenderTarget.hpp>

#include <memory>

TileRenderer::TileRenderer(TileRendererData& rendererdata) noexcept
: data(std::addressof(rendererdata)), texImpl()
{

}

void TileRenderer::setTextureImpl(TextureImpl& textureImpl) noexcept
{
    texImpl = std::addressof(textureImpl);
}

void TileRenderer::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    auto& old = states.texture;
    for(auto& arr : data->vertexArrays)
    {
        const std::string& side = arr.first;
        sf::VertexArray& vertices = arr.second;

        sf::Vector2f off;
        if(texImpl && texImpl->has(side))
        {
            states.texture = texImpl->tex;

            off = texImpl->map[side];
            for(std::size_t i = 0U; i < vertices.getVertexCount(); ++i)
                vertices[i].texCoords = (vertices[i].texCoords + off);
        }
        else
            states.texture = old;
        target.draw(vertices, states);

        for(std::size_t i = 0U; i < vertices.getVertexCount(); ++i)
            vertices[i].texCoords = (vertices[i].texCoords - off);
    }
}

void TileRendererData::set(const TilesetDataType& t) noexcept
{
    for(const auto& sideimpl : t.sides)
    {
        auto& side = sideimpl.second;
        sf::VertexArray arr(sf::PrimitiveType::TrianglesFan, side.size());
        for(std::size_t i = 0U; i < side.size(); ++i)
        {
            sf::Vector2f pos = static_cast<sf::Vector2f>(side[i] - t.ref);
            arr[i] = sf::Vertex(pos);
            arr[i].texCoords = pos; //relative to 0, no old Inkscape-coord
        }
        vertexArrays[sideimpl.first] = arr;
    }
}
