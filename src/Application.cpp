/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "Application.hpp"

#include "StateMachine.hpp"
#include "GameState.hpp"
#include "Config.hpp"

#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include <SFML/Window/Event.hpp>

#include <SFGUI/SFGUI.hpp>
#include <SFGUI/Widgets.hpp>

#include <stdexcept>


Application::Application() noexcept
{

}

ApplicationResult Application::run()
{
    StateMachine machine;
    machine.rememberState<GameState>("game");
    machine.push("game");

    const std::size_t width = Config::Application::width;
    const std::size_t height = Config::Application::height;
    const std::size_t bits = Config::Application::antialiasing;
    const std::string title = Config::Application::title;

    sf::RenderWindow renderApp(sf::VideoMode(width, height, bits),
                               title, sf::Style::None);
    renderApp.setVerticalSyncEnabled(Config::Application::verticalSync);

    while(renderApp.isOpen())
    {
        sf::Event event;
        while(renderApp.pollEvent(event))
        {
            if(event.type == sf::Event::Closed
            || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
            {
                renderApp.close();
            }
            machine.handleEvent(event);
        }
        auto mouse = sf::Mouse::getPosition(renderApp);
        machine.handleMousePos(renderApp, mouse);
        machine.update(sf::seconds(1.f / 60.f));

        renderApp.clear();

        renderApp.draw(machine);

        renderApp.display();
    }

    return ApplicationResult::Normal;
}


