/*  Copyright ²016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "isometric/Entity.hpp"
#include "isometric/EntityInformation.hpp"
#include "isometric/EntityUpdater.hpp"
#include "isometric/Math.hpp"

#include "ScriptingManager.hpp"

#include <SFML/Graphics/RenderTarget.hpp>

#include <SFML/System/Time.hpp>

#include <cassert>

Entity::Entity(EntityInformation& info) noexcept
: mappos(0, 0, 0), z_slab(0), myInfo(std::addressof(info)), spr()
{
    spr.setTexture(*myInfo->textureimpl.tex);
    spr.setTextureRect(myInfo->textureimpl.rect);
}

std::string Entity::name() const noexcept
{
    assert(myInfo);

    return myInfo->name;
}

EntityInformation& Entity::info() const noexcept
{
    assert(myInfo);

    return *myInfo;
}

void Entity::setMapPosition(const sf::Vector3i& pos) noexcept
{
    mappos = pos;

    updatePos();
}

const sf::Vector3i& Entity::getMapPosition() const noexcept
{
    return mappos;
}

int Entity::getZSlab() const noexcept
{
    return z_slab;
}

void Entity::increaseSlab() noexcept
{
    if(z_slab < 4)
        ++z_slab;

    updatePos();
}

void Entity::decreaseSlab() noexcept
{
    if(z_slab > 0)
        --z_slab;

    updatePos();
}

void Entity::onMoveDone() noexcept
{
    for(const auto& a : myInfo->reactions_done)
        scriptingManager.run(a.first);
    updatePos();
}

void Entity::onMoveFail() noexcept
{
    for(const auto& a : myInfo->reactions_fail)
        scriptingManager.run(a.first);
}

void Entity::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(spr, states);
}

void Entity::updatePos() noexcept
{
    const float z = mappos.z + (z_slab / 4.f);
    auto tmp = static_cast<sf::Vector2f>(iso::fromMap({mappos.x, mappos.y},
                                                      z));
    setPosition(tmp);
}
