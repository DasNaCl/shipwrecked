/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "isometric/TextureManager.hpp"

TextureManager::TextureManager() noexcept
: factories(), textures()
{
    
}

TextureManager::TextureManager(
    const std::vector<std::pair<TextureKey, std::string>>& list) noexcept
: factories(), textures()
{
    for(const auto& entry : list)
        add(entry.first, entry.second);
}

void TextureManager::add(TextureKey key, std::string path) noexcept
{
    if(factories.find(key) != factories.end())
        return;
    
    factories[key] = [=](sf::Texture& tex)
        {
            return tex.loadFromFile(path);
        };
}

sf::Texture& TextureManager::get(TextureKey key)
{
    if(textures.find(key) == textures.end())
    {
        if(factories.find(key) == factories.end())
            throw std::invalid_argument("Factory not registered");

        textures[key] = sf::Texture();
        if(!((factories[key])(textures[key])))
            throw std::logic_error("Could not load file");
    }

    return textures[key];
}
