/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "isometric/IsometricMap.hpp"
#include "isometric/EntityInformationLoader.hpp"
#include "isometric/EntityUpdater.hpp"
#include "isometric/TileResource.hpp"
#include "isometric/Entity.hpp"
#include "isometric/Math.hpp"

#include "WorldGenerationManager.hpp"
#include "ScriptInformation.hpp"
#include "TilesetReader.hpp"
#include "KeyWatch.hpp"
#include "Config.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <utility>
#include <cassert>

IsometricMap::IsometricMap(sf::Vector2u size) noexcept
: data(), dim(size),
  player(), tilesetData(), entities(), path(),
  renderHelper(data, entities),
  entityUpdateRate(sf::seconds(0.1f)), total(sf::Time::Zero),
  tileResource(Config::Common::tileResourceFile),
  entityDummies(EntityInformationLoader().get())
{
    const std::vector<std::string> dd = {"Block1", "Block2", "Block3", "Block4"};
    for(const auto& a : dd)
    {
        tilesetData.add(a);
        tilesetData.addSide(a, "left");
        tilesetData.addSide(a, "right");
        tilesetData.addSide(a, "top");
    }
    TilesetReader(tilesetData, "res/tileset.ships");

    WorldGenerationManager wd(size, data, tilesetData, tileResource);
    player = wd.spawnPoint();
    renderHelper.update(player);
}

sf::Vector3i IsometricMap::playerPos() const noexcept
{
    return player;
}

std::vector<EntityInformation>& IsometricMap::entityTypes() noexcept
{
    return entityDummies;
}

void IsometricMap::update(const sf::Time& dt, const sf::Vector3i& playerpos,
                          const sf::Vector2i& mouseWorldPos,
                          const sf::Vector2i& mouseViewPos,
                          const sf::Vector2i& mousePixelPos) noexcept
{
    total += dt;
    if(total >= entityUpdateRate)
    {
        // a bit too much magic keyboard keys here

        int pressed = KeyWatcher.pressed(sf::Keyboard::Left) ? 1 : 0;
        pressed |= KeyWatcher.pressed(sf::Keyboard::Right) ? 1 << 1 : 0;
        pressed |= KeyWatcher.pressed(sf::Keyboard::Down) ? 1 << 2 : 0;
        pressed |= KeyWatcher.pressed(sf::Keyboard::Up) ? 1 << 3 : 0;

        ScriptInformation inf;
        inf.set(static_cast<ScriptInformation::Direction>(pressed));

        EntityUpdater(data, entities, dt, tileResource, inf);

        total = sf::Time::Zero;
    }
    const bool clear = (player != playerpos);
    player = playerpos;

    if(clear)
        path.clear();
    renderHelper.update(player);
}

void IsometricMap::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    target.draw(renderHelper, states);
}
