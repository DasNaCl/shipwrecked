/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "isometric/Tile.hpp"
#include "isometric/Math.hpp"

#include <SFML/Graphics/RenderTarget.hpp>

int Tile::width() noexcept
{
    return 64; //Magic Number!! >:(
}

int Tile::height() noexcept
{
    return 64; //Magic Number!! >:(
}

Tile::Tile(const std::string& type, TileRenderer tileRenderer,
           const TextureImpl& defaultTextures) noexcept
: mappos(0, 0, 0), renderer(tileRenderer), key(type), textures(defaultTextures)
{
    //NOTE: In future versions it might be possible that TextureImpl's
    //      state is changed later on (i.e. player removes stones)
    //      Thats why setTextureImpl wants a reference to our impl

    if(textures.tex)
        renderer.setTextureImpl(textures);
}

void Tile::setMapPosition(const sf::Vector3i& pos) noexcept
{
    mappos = pos;
    updatePos();
}

const sf::Vector3i& Tile::getMapPosition() const noexcept
{
    return mappos;
}

void Tile::setOffset(const sf::Vector2f& off) noexcept
{
    textures.set(off);
}

std::string Tile::getKey() const noexcept
{
    return key;
}

void Tile::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(renderer, states);
}

void Tile::updatePos() noexcept
{
    auto tmp = static_cast<sf::Vector2f>(iso::fromMap({mappos.x, mappos.y},
                                                      mappos.z));
    setPosition(tmp);
}
