/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "isometric/Math.hpp"
#include "isometric/Tile.hpp"

#include <cmath>

namespace iso
{
    sf::Vector2i fromMap(sf::Vector2i pos, int z) noexcept
    {
        const float w = Tile::width();
        const float h = Tile::height();

        const float x = std::round((pos.x - pos.y) * (w / 2.f));
        const float y = std::round((pos.x + pos.y) * (h / 4.f));

        return {static_cast<int>(x),
                static_cast<int>(y - z * (h / 2.f))};
    }

    sf::Vector2i toMap(sf::Vector2i pos, int z) noexcept
    {
        const float w = Tile::width();
        const float h = Tile::height();

        const float first = (2 * pos.y) / h;
        const float second = (pos.x / w);
        
        return {static_cast<int>(std::round(first + second)),
                static_cast<int>(std::round(first - second))};
    }

    sf::Vector2i fromMap(sf::Vector2i pos, float z) noexcept
    {
        const float w = Tile::width();
        const float h = Tile::height();

        const float x = std::round((pos.x - pos.y) * (w / 2.f));
        const float y = std::round((pos.x + pos.y) * (h / 4.f));

        return {static_cast<int>(x),
                static_cast<int>(y - std::round(z * (h / 2.f)))};
    }

    sf::Vector2i toMap(sf::Vector2i pos, float z) noexcept
    {
        const float w = Tile::width();
        const float h = Tile::height();

        const float first = (2.f * pos.y) / h;
        const float second = (pos.x / w);

        return {static_cast<int>(std::round(first + second)),
                static_cast<int>(std::round(first - second))};
    }
}
