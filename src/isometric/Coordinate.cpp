/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "isometric/Coordinate.hpp"

#include <cassert>

Coordinate::Coordinate() noexcept
: x(), y(), z()
{
    
}

Coordinate::Coordinate(int X, int Y, int Z) noexcept
: x(X), y(Y), z(Z)
{

}

Coordinate Coordinate::operator=(const sf::Vector3i& vec) const noexcept
{
    return {vec.x, vec.y, vec.z};
}


bool operator<(const Coordinate& lhs, const Coordinate& rhs) noexcept
{
    return (lhs.z < rhs.z)
        || (lhs.z == rhs.z && (lhs.x < rhs.x
                           || (lhs.x == rhs.x && lhs.y < rhs.y)));
}

bool operator==(const Coordinate& lhs,
                const Coordinate& rhs) noexcept
{
    return (lhs.z == rhs.z && lhs.x == rhs.x && lhs.y == lhs.y);
}
