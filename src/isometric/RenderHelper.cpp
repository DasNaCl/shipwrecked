/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "isometric/RenderHelper.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

#include <algorithm>
#include <cassert>

int RenderHelper::width = 16;
int RenderHelper::height = 6;
int RenderHelper::depth = 16;

RenderHelper::RenderHelper(std::map<Coordinate, Tile>& mapTiles,
                           std::vector<Entity::Ptr>& mapEntities) noexcept
: mapDataTiles(mapTiles), mapDataEntities(mapEntities), player()
{

}

void RenderHelper::update(const sf::Vector3i& center) noexcept
{
    player = center;
}

void RenderHelper::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    const sf::Vector3i from{player.x - width/2, player.y - depth/2,
            player.z - height/2};
    const sf::Vector3i to{player.x + width/2, player.y + depth/2,
            player.z + height/2};

    for(int z = from.z; z <= to.z; ++z)
    {
        for(int y = from.y; y <= to.y; ++y)
        {
            for(int x = from.x; x <= to.x; ++x)
            {
                const sf::Vector3i cur(x, y, z);

                auto mit = mapDataTiles.find({x, y, z});
                if(mit != mapDataTiles.end())
                    target.draw(mit->second, states);

                auto vit = std::find_if(mapDataEntities.begin(),
                                        mapDataEntities.end(),
                                        [cur](const Entity::Ptr& lhs)
                                        { return lhs->getMapPosition() == cur;});
                if(vit != mapDataEntities.end())
                    target.draw(**vit, states);
            }
        }
    }
}
