/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "isometric/TileResource.hpp"

#include "angelscript/angelscript.hpp"
#include "angelscript/TextureImpl.hpp"
#include "ScriptingManager.hpp"

#include <SFML/Graphics/Texture.hpp>

#include <cassert>

TileResource::TileResource(std::string filepath)
: data(), texmanager({{TextureKey::Terrain, "res/terrain.png"}})
{
    if(filepath.empty())
        loadStandard();
    else
        loadFromFile(filepath);
}

bool TileResource::allowsMovement(const std::string& key, Entity& e,
                                  const sf::Vector3i& to) const noexcept
{
    auto& t = getTileInformation(key);
    for(auto func : t.rulesets)
        if(!func(e, to))
            return false;
    return true;
}

const TextureImpl& TileResource::defaultTextures(const std::string& key) const noexcept
{
    return getTileInformation(key).defaultTextures;
}

int TileResource::zSlabHeight(const std::string& key) const noexcept
{
    return getTileInformation(key).zSlabHeight;
}

MovementType TileResource::movementType(const std::string& key) const noexcept
{
    return getTileInformation(key).movementType;
}

const TileInformation& TileResource::getTileInformation(const std::string& key) const noexcept
{
    auto it = data.find(key);
    assert(it != data.end());

    return it->second;
}

void TileResource::loadStandard()
{
    initScripting();

    scriptingManager.add("tileResourceStandard", "tileResource",
                         "void main()"
                         "{"
                         "    TextureImpl impl;"
                         "    impl.push(\"left\");"
                         "    impl.push(\"right\");"
                         "    impl.push(\"top\");"
                         "    "
                         "    impl.setAll(0, 0);"
                         "    tileResource.add(\"Block4\", 0, 1, impl);"
                         "    impl.setAll(64, 0);"
                         "    tileResource.add(\"Block3\", 3, 1, impl);"
                         "    impl.setAll(128, 0);"
                         "    tileResource.add(\"Block2\", 2, 1, impl);"
                         "    impl.setAll(194, 0);"
                         "    tileResource.add(\"Block1\", 1, 1, impl);"
                         "}");
    scriptingManager.run("tileResourceStandard");

    add("Air", 0, (std::size_t)MovementType::Normal, angelscript::TextureImpl());
}

void TileResource::initScripting()
{
    auto* engine = scriptingManager.engine();

    int r = engine->RegisterObjectType("TextureImpl",
                                       sizeof(angelscript::TextureImpl),
                                       asOBJ_VALUE);
    assert(r >= 0);
    r = engine->RegisterObjectBehaviour("TextureImpl", asBEHAVE_CONSTRUCT,
                                    "void f()",
                                    asFUNCTION(angelscript::textureimpl_construct),
                                    asCALL_CDECL_OBJLAST);
    assert(r >= 0);
    r = engine->RegisterObjectBehaviour("TextureImpl", asBEHAVE_DESTRUCT,
                                     "void f()",
                                     asFUNCTION(angelscript::textureimpl_destruct),
                                     asCALL_CDECL_OBJLAST);
    assert(r >= 0);
    r = engine->RegisterObjectMethod("TextureImpl",
                                 "void opAssign(const TextureImpl &in)",
                                 asMETHODPR(angelscript::TextureImpl,
                                            operator=,
                                            (const angelscript::TextureImpl&),
                                            angelscript::TextureImpl&),
                                 asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterObjectMethod("TextureImpl", "bool has(string)",
                                     asFUNCTION(angelscript::textureimpl_has),
                                     asCALL_CDECL_OBJLAST);
    assert(r >= 0);
    r = engine->RegisterObjectMethod("TextureImpl", "void set(string,uint,uint)",
                                     asFUNCTION(angelscript::textureimpl_set),
                                     asCALL_CDECL_OBJLAST);
    assert(r >= 0);
    r = engine->RegisterObjectMethod("TextureImpl", "void setAll(uint,uint)",
                                     asFUNCTION(angelscript::textureimpl_setAll),
                                     asCALL_CDECL_OBJLAST);
    assert(r >= 0);
    r = engine->RegisterObjectMethod("TextureImpl", "void push(string)",
                                     asFUNCTION(angelscript::textureimpl_push),
                                     asCALL_CDECL_OBJLAST);
    assert(r >= 0);
    r = engine->RegisterObjectMethod("TextureImpl", "void clear()",
                                     asFUNCTION(angelscript::textureimpl_clear),
                                     asCALL_CDECL_OBJLAST);
    assert(r >= 0);    

    r = engine->RegisterObjectType("TileResource", 0, asOBJ_REF);
    assert(r >= 0);
    r = engine->RegisterObjectBehaviour("TileResource", asBEHAVE_ADDREF,
                                        "void addRef()",
                                        asMETHOD(TileResource, addRef),
                                        asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterObjectBehaviour("TileResource", asBEHAVE_RELEASE,
                                        "void releaseRef()",
                                        asMETHOD(TileResource, releaseRef),
                                        asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterGlobalProperty("TileResource tileResource", this);
    assert(r >= 0);

    r = engine->RegisterObjectMethod("TileResource",
                                     "void add(string,uint,uint,TextureImpl &in)",
                                     asMETHOD(TileResource, add),
                                     asCALL_THISCALL);
    assert(r >= 0);
}

void TileResource::add(const std::string& tileid, unsigned int zheight,
                       unsigned int movementType,
                       const angelscript::TextureImpl& pim) noexcept
{
    sf::Texture& tex = texmanager.get(TextureKey::Terrain);
    TextureImpl impl(tex, pim);
    data.emplace(tileid, TileInformation{tileid, static_cast<int>(zheight),
                 static_cast<MovementType>(movementType), impl});
}

void TileResource::loadFromFile(const std::string& filepath)
{
    //implement
    assert(false);
}
