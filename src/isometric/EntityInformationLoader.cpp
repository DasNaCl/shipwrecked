/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "isometric/EntityInformationLoader.hpp"
#include "isometric/TextureManager.hpp"

#include "json/json.h"

#include "Config.hpp"

#include <stdexcept>
#include <fstream>

#include <glob.h>

EntityInformationLoader::EntityInformationLoader()
: entityTypes()
{
    std::vector<std::string> files;
    {
    const std::string pattern = Config::Entities::path + "*.json";
    glob_t glob_result;
    if(glob(pattern.c_str(), GLOB_TILDE, NULL, &glob_result) != 0)
        throw std::runtime_error("No entities found!");
    for(std::size_t i = 0U; i < glob_result.gl_pathc; ++i)
        files.push_back(std::string(glob_result.gl_pathv[i]));
    globfree(&glob_result);
    }

    entityTypes.reserve(files.size());
    for(auto& p : files)
        load(p);
}

std::vector<EntityInformation> EntityInformationLoader::get() const noexcept
{
    return entityTypes;
}

void EntityInformationLoader::load(const std::string& path) noexcept
{
    Json::Value root;
    {
        std::ifstream _if(path);
        Json::CharReaderBuilder rbuilder;

        std::string errs;
        bool parsingSuccessful = Json::parseFromStream(rbuilder, _if,
                                                       &root, &errs);
        if (!parsingSuccessful)
            throw std::runtime_error("Could not parse JSON file \"" + path
                + "\". err: " + errs);
    }
    
    int left = root["texture"]["left"].asInt();
    int top = root["texture"]["top"].asInt();
    int width = root["texture"]["width"].asInt();
    int height = root["texture"]["height"].asInt();

    auto& texmanager = Config::Common::texmanager;
    EntityTextureImpl tex(texmanager.get(TextureKey::Entities),
                          sf::IntRect(left, top, width, height));
    std::map<std::string, std::string> behaviors;
    for(std::size_t i = 0U; i < root["behaviors"].size(); ++i)
    {
        {
        Json::Value::ArrayIndex index = i;
        const std::string behavior = root["behaviors"][index].asString();
        std::ifstream _if("entities/behaviors/" + behavior);
        std::string buf;
        while(std::getline(_if, buf))
            behaviors[behavior] += buf + "\n";
        }
    }

    std::map<std::string, std::string> reactions_done;
    for(std::size_t i = 0U; i < root["reactions"]["done"].size(); ++i)
    {
        {
        Json::Value::ArrayIndex index = i;
        const std::string behavior = root["reactions"]["done"][index].asString();
        std::ifstream _if("entities/reactions/done/" + behavior);
        std::string buf;
        while(std::getline(_if, buf))
            reactions_done[behavior] += buf + "\n";
        }
    }

    std::map<std::string, std::string> reactions_fail;
    for(std::size_t i = 0U; i < root["reactions"]["fail"].size(); ++i)
    {
        {
        Json::Value::ArrayIndex index = i;
        const std::string behavior = root["reactions"]["fail"][index].asString();
        std::ifstream _if("entities/reactions/fail/" + behavior);
        std::string buf;
        while(std::getline(_if, buf))
            reactions_fail[behavior] += buf + "\n";
        }
    }

    entityTypes.push_back({root["name"].asString(),
                root["lifepoints"].asDouble(),
                tex, behaviors, reactions_done, reactions_fail});
}
