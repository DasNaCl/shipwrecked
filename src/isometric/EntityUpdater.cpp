/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "isometric/EntityUpdater.hpp"
#include "isometric/EntityInformation.hpp"
#include "isometric/TileResource.hpp"

#include "angelscript/angelscript.hpp"
#include "ScriptInformation.hpp"
#include "ScriptingManager.hpp"
#include "ScriptEntity.hpp"

#include <cassert>
#include <set>

bool EntityUpdater::scriptingRegistered = false;

static ScriptEntity scriptEntity;
static ScriptInformation scriptInformation;

static std::set<std::string> registered_behaviors;
static std::set<std::string> registered_done;
static std::set<std::string> registered_fail;

EntityUpdater::EntityUpdater(const std::map<Coordinate, Tile>& data,
                             const std::vector<Entity::Ptr>& entities,
                             const sf::Time& dt,
                             TileResource& tileResourceParam,
                             const ScriptInformation& input) noexcept
: tiles(data), tileResource(tileResourceParam)
{
    if(!scriptingRegistered)
    {
        registerScripting();

        for(auto& en : entities)
        {
            auto myInfo = en->info();
            for(const auto& a : myInfo.behaviors)
            {
                if(registered_behaviors.find(a.first) == registered_behaviors.end())
                {
                    scriptingManager.add(a.first, "behavior", a.second);
                    registered_behaviors.insert(a.first);
                }
            }
            for(const auto& a : myInfo.reactions_done)
            {
                if(registered_done.find(a.first) == registered_done.end())
                {
                    scriptingManager.add(a.first, "reactions_done", a.second);
                    registered_done.insert(a.first);
                }
            }
            for(const auto& a : myInfo.reactions_fail)
            {
                if(registered_fail.find(a.first) == registered_fail.end())
                {
                    scriptingManager.add(a.first, "reactions_fail", a.second);
                    registered_fail.insert(a.first);
                }
            }
        }

        scriptingRegistered = true;        
    }

    scriptInformation = input;

    for(auto it = entities.begin(); it != entities.end(); ++it)
    {
        scriptEntity.set(sf::Vector3i());
        auto& info = (*it)->info();

        for(const auto& b : info.behaviors)
            scriptingManager.run(b.first);
        sf::Vector3i req = scriptEntity.moveRequest();

        const int sum = req.x + req.y + req.z;
        if(sum == 0)
            continue;
        const sf::Vector3i entityPos = (*it)->getMapPosition();
        sf::Vector3i to = entityPos + req;
        auto itd = tiles.find({entityPos.x, entityPos.y, entityPos.z-1});
        auto key = (itd != tiles.end() ? itd->second.getKey() : "Air");

        if(tileResource.allowsMovement(key, (**it), to))
        {
            (*it)->setMapPosition(to);
            (*it)->onMoveDone();
        }
        else
            (*it)->onMoveFail();
        updateSlab(**it);
    }
}

void EntityUpdater::updateSlab(Entity& e) const noexcept
{
    sf::Vector3i pos = e.getMapPosition();

    auto itt = tiles.find({pos.x, pos.y, pos.z});

    int level = 0;
    if(itt != tiles.end())
        level = tileResource.zSlabHeight(itt->second.getKey());

    int total = level - e.getZSlab();
    while(e.getZSlab() != level)
    {
        if(total < 0)
            e.decreaseSlab();
        else
            e.increaseSlab();

        total = level - e.getZSlab();
    }
}

void EntityUpdater::registerScripting() noexcept
{
    if(scriptingRegistered)
        return;

    auto* engine = scriptingManager.engine();

    int r = engine->RegisterObjectType("ScriptEntity", 0, asOBJ_REF);
    assert(r >= 0);
    r = engine->RegisterObjectBehaviour("ScriptEntity", asBEHAVE_ADDREF,
                                        "void addRef()",
                                        asMETHOD(ScriptEntity, addRef),
                                        asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterObjectBehaviour("ScriptEntity", asBEHAVE_RELEASE,
                                        "void rmRef()",
                                        asMETHOD(ScriptEntity, rmRef),
                                        asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterObjectMethod("ScriptEntity", "void moveX(int x)",
                                     asMETHOD(ScriptEntity, moveX),
                                     asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterObjectMethod("ScriptEntity", "void moveY(int y)",
                                     asMETHOD(ScriptEntity, moveY),
                                     asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterObjectMethod("ScriptEntity", "void moveZ(int z)",
                                     asMETHOD(ScriptEntity, moveZ),
                                     asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterGlobalProperty("ScriptEntity entity", &scriptEntity);
    assert(r >= 0);

    r = engine->RegisterObjectType("ScriptInformation", 0, asOBJ_REF);
    assert(r >= 0);
    r = engine->RegisterObjectBehaviour("ScriptInformation", asBEHAVE_ADDREF,
                                        "void addRef()",
                                        asMETHOD(ScriptInformation, addRef),
                                        asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterObjectBehaviour("ScriptInformation", asBEHAVE_RELEASE,
                                        "void rmRef()",
                                        asMETHOD(ScriptInformation, rmRef),
                                        asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterObjectMethod("ScriptInformation", "bool leftPressed()",
                                     asMETHOD(ScriptInformation, leftPressed),
                                     asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterObjectMethod("ScriptInformation", "bool rightPressed()",
                                     asMETHOD(ScriptInformation, rightPressed),
                                     asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterObjectMethod("ScriptInformation", "bool upPressed()",
                                     asMETHOD(ScriptInformation, upPressed),
                                     asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterObjectMethod("ScriptInformation", "bool downPressed()",
                                     asMETHOD(ScriptInformation, downPressed),
                                     asCALL_THISCALL);
    assert(r >= 0);
    r = engine->RegisterGlobalProperty("ScriptInformation information",
                                       &scriptInformation);
    assert(r >= 0);
}
