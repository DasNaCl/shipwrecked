/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "TilesetData.hpp"

#include <cassert>

void TilesetData::add(const std::string& type) noexcept
{
    data.insert(std::make_pair(type, TilesetDataType(type)));
}

void TilesetData::addSide(const std::string& type,
                          const std::string& side) noexcept
{
    auto it = data.find(type);
    assert(it != data.end());
    try
    {
    it->second.sides.insert(std::make_pair(side, std::vector<sf::Vector2i>()));
    }
    catch(...)
    {
    //We don't care if it fails
    }
}

TileRenderer TilesetData::renderer(const std::string& type) noexcept
{
    auto it = data.find(type);
    assert(it != data.end());
    
    return it->second.renderer();
}

void TilesetData::updateRenderer(const std::string& type) noexcept
{
    auto it = data.find(type);
    assert(it != data.end());

    it->second.tileRendererData.set(it->second);
}

std::vector<std::string> TilesetData::tileTypeSides(const std::string& type) noexcept
{
    auto it = data.find(type);
    assert(it != data.end());
    
    std::vector<std::string> vec;
    vec.reserve(it->second.sides.size());
    for(const auto& a : it->second.sides)
        vec.push_back(a.first);
    return vec;
}

bool TilesetData::hasTileTypes() const noexcept
{
    return !(data.empty());
}

std::vector<std::string> TilesetData::tileTypes() const noexcept
{
    std::vector<std::string> vec;
    vec.reserve(data.size());
    for(const auto& a : data)
        vec.push_back(a.first);
    return vec;
}

TilesetDataType& TilesetData::operator[](const std::string& type) noexcept
{
    auto it = data.find(type);
    assert(it != data.end());

    return it->second;
}

const TilesetDataType& TilesetData::operator[](const std::string& type) const noexcept
{
    auto it = data.find(type);
    assert(it != data.end());
    
    return it->second;
}
