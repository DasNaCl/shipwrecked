/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "GameState.hpp"
#include "EntityTextureImpl.hpp"
#include "TilesetReader.hpp"
#include "Application.hpp"
#include "TilesetData.hpp"
#include "Config.hpp"

#include "isometric/TileRenderer.hpp"
#include "isometric/Math.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include <SFML/System/Time.hpp>

#include <SFML/Window/Event.hpp>

#include <cassert>

GameState::GameState(StateMachine& m)
: State(m), isomap({8, 8}), view(sf::FloatRect(0.f, 0.f, 800.f, 600.f)),
  mousePosition(), sfguiRenderTexture(), gameviewRenderTexture(),
  gui()
{
    const std::size_t width = Config::Application::width;
    const std::size_t height = Config::Application::height;

    if(!sfguiRenderTexture.create(width * 0.2, height))
        throw std::runtime_error("Couldn\'t create sf::RenderTexture!");
    sfguiRenderTexture.resetGLStates();

    if(!gameviewRenderTexture.create(width * 0.8, height))
        throw std::runtime_error("Couldn\'t create sf::RenderTexture!");

    bool hasPlayer = false;
    for(auto& e : isomap.entityTypes())
    {
        if(e.name == "player")
        {
            player = dynamic_cast<Entity*>(std::addressof(isomap.add([&e]()
            { return std::make_unique<Entity>(e); })));

            // Spawn
            player->setMapPosition(isomap.playerPos());
            hasPlayer = true;
        }
    }

    assert(player); assert(hasPlayer);
    view.setCenter(player->getPosition());
}

State::Strategy GameState::update(const sf::Time& dt) noexcept
{
    gui.update(dt);

    auto oldpos = player->getMapPosition();
    isomap.update(dt, player->getMapPosition(),
                  mousePosition.world,
                  mousePosition.view,
                  mousePosition.pixel);
    auto newpos = player->getMapPosition();

    auto delta = newpos - oldpos;
    const auto diff = iso::fromMap({delta.x, delta.y}, delta.z);
    view.move(diff.x, diff.y);

    return State::Strategy::Nothing;
}

State::Strategy GameState::handleEvent(const sf::Event& event) noexcept
{
    //sneaky hack, as we draw the gui in a move renderTexture,
    //we have to adjust the mouseclick-events
    const std::size_t width = Config::Application::width;
    const std::size_t height = Config::Application::height;

    const sf::Vector2i offset(static_cast<int>(width * 0.8f), 0);
    sf::Event cpy = event;
    switch(cpy.type)
    {
    case sf::Event::MouseButtonPressed:
    case sf::Event::MouseButtonReleased:
        cpy.mouseButton.x -= offset.x;
        cpy.mouseButton.y -= offset.y;
        break;
    case sf::Event::MouseMoved:
    case sf::Event::MouseEntered:
    case sf::Event::MouseLeft:
        cpy.mouseMove.x -= offset.x;
        cpy.mouseMove.y -= offset.y;
        break;
    case sf::Event::MouseWheelMoved:
        cpy.mouseWheel.x -= offset.x;
        cpy.mouseWheel.y -= offset.y;
        break;
    case sf::Event::MouseWheelScrolled:
        cpy.mouseWheelScroll.x -= offset.x;
        cpy.mouseWheelScroll.y -= offset.y;
        break;
    }
    gui.handleEvent(cpy);
    return State::Strategy::Nothing;
}

State::Strategy GameState::handleMousePos(const sf::Vector2i& mouse,
                                          const sf::RenderTarget& target) noexcept
{
    const auto v = gameviewRenderTexture.mapPixelToCoords(mouse, view);
    mousePosition.view = static_cast<sf::Vector2i>(v);
    mousePosition.world = iso::toMap(mousePosition.view, 0);
    mousePosition.pixel = mouse;

    --(mousePosition.world.x);

    return State::Strategy::Nothing;
}

void GameState::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    const std::size_t width = Config::Application::width;
    states.transform *= getTransform();

    sfguiRenderTexture.clear();

    gui.draw(sfguiRenderTexture);

    sfguiRenderTexture.display();

    gameviewRenderTexture.clear();
    gameviewRenderTexture.setView(view);

    gameviewRenderTexture.draw(isomap);

    gameviewRenderTexture.display();

    target.draw(sf::Sprite(gameviewRenderTexture.getTexture()));

    sf::Sprite spr(sfguiRenderTexture.getTexture());
    spr.setPosition(width * 0.8f, 0.f);
    target.draw(spr);
}
