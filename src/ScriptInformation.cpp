/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "ScriptInformation.hpp"

bool ScriptInformation::leftPressed() const noexcept
{
    auto&& ref = static_cast<int>(pressed_status);
    return !!(ref & static_cast<int>(Direction::LEFT));
}

bool ScriptInformation::rightPressed() const noexcept
{
    auto&& ref = static_cast<int>(pressed_status);
    return !!(ref & static_cast<int>(Direction::RIGHT));
}

bool ScriptInformation::upPressed() const noexcept
{
    auto&& ref = static_cast<int>(pressed_status);
    return !!(ref & static_cast<int>(Direction::UP));
}

bool ScriptInformation::downPressed() const noexcept
{
    auto&& ref = static_cast<int>(pressed_status);
    return !!(ref & static_cast<int>(Direction::DOWN));
}

void ScriptInformation::set(ScriptInformation::Direction dir) noexcept
{
    pressed_status = dir;
}
