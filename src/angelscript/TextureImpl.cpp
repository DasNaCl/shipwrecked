/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "angelscript/TextureImpl.hpp"

namespace angelscript
{

TextureImpl::TextureImpl() noexcept
: map()
{

}

TextureImpl::TextureImpl(const TextureImpl& impl) noexcept
: map(impl.map)
{

}

TextureImpl& TextureImpl::operator=(const TextureImpl& other) noexcept
{
    map = other.map;

    return *this;
}

void textureimpl_construct(void* mem)
{
    new (mem) TextureImpl();
}

void textureimpl_destruct(void* mem)
{
    reinterpret_cast<TextureImpl*>(mem)->~TextureImpl();
}

bool textureimpl_has(const std::string& side, TextureImpl* impl) noexcept
{
    return (impl->map.find(side) != impl->map.end());
}

void textureimpl_set(const std::string& side,
                     unsigned int offX, unsigned int offY,
                     TextureImpl* impl) noexcept
{
    impl->map[side] = std::make_pair(offX, offY);
}

void textureimpl_setAll(unsigned int offX, unsigned int offY,
                        TextureImpl* impl) noexcept
{
    for(auto& a : impl->map)
        a.second = std::make_pair(offX, offY);
}

void textureimpl_push(const std::string& side, TextureImpl* impl) noexcept
{
    impl->map.insert(std::make_pair(side, TextureImpl::Vec2()));
}

void textureimpl_clear(TextureImpl* impl) noexcept
{
    impl->map.clear();
}

bool TextureImpl::has(const std::string& side) noexcept
{
    return textureimpl_has(side, this);
}

void TextureImpl::set(const std::string& side,
                      unsigned int offX, unsigned int offY) noexcept
{
    textureimpl_set(side, offX, offY, this);
}

void TextureImpl::setAll(unsigned int offX, unsigned int offY) noexcept
{
    textureimpl_setAll(offX, offY, this);
}

void TextureImpl::push(const std::string& side) noexcept
{
    textureimpl_push(side, this);
}

void TextureImpl::clear() noexcept
{
    textureimpl_clear(this);
}
    
std::map<std::string, sf::Vector2f> TextureImpl::data() const noexcept
{
    std::map<std::string, sf::Vector2f> toReturn;
    for(auto&& a : map)
        toReturn[a.first] = sf::Vector2f(a.second.first, a.second.second);
    return toReturn;
}

}
