/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "TextureImpl.hpp"

#include "isometric/TextureManager.hpp"
#include "angelscript/TextureImpl.hpp"

#include <memory>

TextureImpl::TextureImpl(sf::Texture& texture) noexcept
: map(), tex(std::addressof(texture))
{

}

TextureImpl::TextureImpl(sf::Texture& texture,
                         const angelscript::TextureImpl& impl) noexcept
: map(impl.data()), tex(std::addressof(texture))
{

}

bool TextureImpl::has(const std::string& side) const noexcept
{
    return (map.find(side) != map.end());
}

void TextureImpl::set(const std::string& side, const sf::Vector2f& off) noexcept
{
    map[side] = off;
}

void TextureImpl::set(const sf::Vector2f& off) noexcept
{
    for(auto& a : map)
        a.second = off;
}

void TextureImpl::clear() noexcept
{
    map.clear();
}
