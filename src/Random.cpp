/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "Random.hpp"
#include "ScriptingManager.hpp"

#include <chrono>

Random::Random() noexcept
: mt(std::chrono::high_resolution_clock::now().time_since_epoch()
                                              .count())
{
      
}

void Random::seed(std::uint_fast32_t seed) noexcept
{
    mt.seed(seed);
}

std::mt19937& Random::gen() noexcept
{
    return mt;
}
