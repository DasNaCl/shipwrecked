/*  Copyright 2016 Matthis Kruse
 * 
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "Config.hpp"

#include "isometric/EntityInformationLoader.hpp"

std::string Config::Common::configFile = "res/config.json";
std::string Config::Common::tileResourceFile = "";
std::string Config::Common::worldgenerationFile = "res/worldgenerators/perlin.as";
TextureManager Config::Common::texmanager({{TextureKey::Entities,
                "res/entities.png"}});

std::string Config::Application::title = "Shipwrecked";
std::size_t Config::Application::width = 800;
std::size_t Config::Application::height = 600;
std::size_t Config::Application::bits = 32;
bool Config::Application::verticalSync = false;
unsigned int Config::Application::antialiasing = 0;

std::size_t Config::World::size = 256;

std::string Config::Entities::path = "entities/";
